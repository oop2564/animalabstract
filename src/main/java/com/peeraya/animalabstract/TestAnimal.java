/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.animalabstract;

/**
 *
 * @author Administrator
 */
public class TestAnimal {
    public static void main(String[] args) {
        //Human
        Human h1 = new Human("Chanya");
        h1.run();
        h1.eat();
        h1.walk();
        h1.speak();
        h1.sleep();
        
        Animal a1 = h1;
        System.out.println("a1 is Animal ? " + (a1 instanceof Animal));
        System.out.println("a1 is Land Animal ? " + (a1 instanceof LandAnimal));
        System.out.println("a1 is Reptile ? " + (a1 instanceof Reptile));
        System.out.println("a1 is Aquatic Animal ? " + (a1 instanceof AquaticAnimal));
        System.out.println("a1 is Poultry ? " + (a1 instanceof Poultry));
        a1.BreakRange();
        
        //Cat
        Cat c1 = new Cat("Makam");
        c1.run();
        c1.eat();
        c1.walk();
        c1.speak();
        c1.sleep();
        
        Animal a2 = c1;
        System.out.println("a2 is Animal ? " + (a2 instanceof Animal));
        System.out.println("a2 is Land Animal ? " + (a2 instanceof LandAnimal));
        System.out.println("a2 is Reptile ? " + (a2 instanceof Reptile));
        System.out.println("a2 is Aquatic Animal ? " + (a2 instanceof AquaticAnimal));
        System.out.println("a2 is Poultry ? " + (a2 instanceof Poultry));
        a2.BreakRange();
        
        //Dog
        Dog d1 = new Dog("Spy");
        d1.run();
        d1.eat();
        d1.walk();
        d1.speak();
        d1.sleep();
        
        Animal a3 = d1;
        System.out.println("a3 is Animal ? " + (a3 instanceof Animal));
        System.out.println("a3 is Land Animal ? " + (a3 instanceof LandAnimal));
        System.out.println("a3 is Reptile ? " + (a3 instanceof Reptile));
        System.out.println("a3 is Aquatic Animal ? " + (a3 instanceof AquaticAnimal));
        System.out.println("a3 is Poultry ? " + (a3 instanceof Poultry));
        a3.BreakRange();
        
        //Snake
        Snake s1 = new Snake("Makrut");
        s1.crawl();
        s1.eat();
        s1.walk();
        s1.speak();
        s1.sleep();
        
        Animal a4 = s1;
        System.out.println("a4 is Animal ? " + (a4 instanceof Animal));
        System.out.println("a4 is Land Animal ? " + (a4 instanceof LandAnimal));
        System.out.println("a4 is Reptile ? " + (a4 instanceof Reptile));
        System.out.println("a4 is Aquatic Animal ? " + (a4 instanceof AquaticAnimal));
        System.out.println("a4 is Poultry ? " + (a4 instanceof Poultry));
        a4.BreakRange();
        
        //Crocodile
        Crocodile cr1 = new Crocodile("Mangkut");
        cr1.crawl();
        cr1.eat();
        cr1.walk();
        cr1.speak();
        cr1.sleep();
        
        Animal a5 = cr1;
        System.out.println("a5 is Animal ? " + (a5 instanceof Animal));
        System.out.println("a5 is Land Animal ? " + (a5 instanceof LandAnimal));
        System.out.println("a5 is Reptile ? " + (a5 instanceof Reptile));
        System.out.println("a5 is Aquatic Animal ? " + (a5 instanceof AquaticAnimal));
        System.out.println("a5 is Poultry ? " + (a5 instanceof Poultry));
        a5.BreakRange();
        
        //Fish
        Fish fish1 = new Fish("Mamuang");
        fish1.swim();
        fish1.eat();
        fish1.walk();
        fish1.speak();
        fish1.sleep();
        
        Animal a6 = fish1;
        System.out.println("a6 is Animal ? " + (a6 instanceof Animal));
        System.out.println("a6 is Land Animal ? " + (a6 instanceof LandAnimal));
        System.out.println("a6 is Reptile ? " + (a6 instanceof Reptile));
        System.out.println("a6 is Aquatic Animal ? " + (a6 instanceof AquaticAnimal));
        System.out.println("a6 is Poultry ? " + (a6 instanceof Poultry));
        a6.BreakRange();
        
        //Crab
        Crab crab1 = new Crab("Mamaew");
        crab1.swim();
        crab1.eat();
        crab1.walk();
        crab1.speak();
        crab1.sleep();
        
        Animal a7 = crab1;
        System.out.println("a7 is Animal ? " + (a7 instanceof Animal));
        System.out.println("a7 is Land Animal ? " + (a7 instanceof LandAnimal));
        System.out.println("a7 is Reptile ? " + (a7 instanceof Reptile));
        System.out.println("a7 is Aquatic Animal ? " + (a7 instanceof AquaticAnimal));
        System.out.println("a7 is Poultry ? " + (a7 instanceof Poultry));
        a7.BreakRange();

        //Bird
        Bird bird1 = new Bird("Mamu");
        bird1.fly();
        bird1.eat();
        bird1.walk();
        bird1.speak();
        bird1.sleep();
        
        Animal a8 = bird1;
        System.out.println("a8 is Animal ? " + (a8 instanceof Animal));
        System.out.println("a8 is Land Animal ? " + (a8 instanceof LandAnimal));
        System.out.println("a8 is Reptile ? " + (a8 instanceof Reptile));
        System.out.println("a8 is Aquatic Animal ? " + (a8 instanceof AquaticAnimal));
        System.out.println("a8 is Poultry ? " + (a8 instanceof Poultry));
        a8.BreakRange();
        
        //Bat
        Bat bat1 = new Bat("Mamad");
        bat1.fly();
        bat1.eat();
        bat1.walk();
        bat1.speak();
        bat1.sleep();
        
        Animal a9 = bat1;
        System.out.println("a9 is Animal ? " + (a9 instanceof Animal));
        System.out.println("a9 is Land Animal ? " + (a9 instanceof LandAnimal));
        System.out.println("a9 is Reptile ? " + (a9 instanceof Reptile));
        System.out.println("a9 is Aquatic Animal ? " + (a9 instanceof AquaticAnimal));
        System.out.println("a9 is Poultry ? " + (a9 instanceof Poultry));
        a9.BreakRange();
        
        //Earthworm
        Earthworm earthworm1 = new Earthworm("Madun");
        earthworm1.run();
        earthworm1.eat();
        earthworm1.walk();
        earthworm1.speak();
        earthworm1.sleep();
        
        Animal a10 = earthworm1;
        System.out.println("a10 is Animal ? " + (a10 instanceof Animal));
        System.out.println("a10 is Land Animal ? " + (a10 instanceof LandAnimal));
        System.out.println("a10 is Reptile ? " + (a10 instanceof Reptile));
        System.out.println("a10 is Aquatic Animal ? " + (a10 instanceof AquaticAnimal));
        System.out.println("a10 is Poultry ? " + (a10 instanceof Poultry));
        a10.BreakRange();
    }
}
