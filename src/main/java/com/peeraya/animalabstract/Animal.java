/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.animalabstract;

/**
 *
 * @author Administrator
 */
public abstract class Animal {

    public String name;
    private int numOfLeg;

    public Animal(String name, int numOfLeg) {
        this.name = name;
        this.numOfLeg = numOfLeg;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumOfLeg() {
        return numOfLeg;
    }

    public void setNumOfLeg(int numOfLeg) {
        this.numOfLeg = numOfLeg;
    }

    @Override
    public String toString() {
        return "Animal{" + "name=" + name + ", numOfLeg=" + numOfLeg + '}';
    }
    
    public void BreakRange() {
        System.out.println("-------------------------");
    }

    public abstract void eat();
    public abstract void walk();
    public abstract void speak();
    public abstract void sleep();
}
