/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.animalabstract;

/**
 *
 * @author Administrator
 */
public abstract class AquaticAnimal extends Animal {

    public AquaticAnimal(String name) {
        super(name, 0);
        System.out.println("I am Aquatic Animal");
    }
    
    public abstract void swim();
}
