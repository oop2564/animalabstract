/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.animalabstract;

/**
 *
 * @author Administrator
 */
public abstract class Reptile extends Animal {

    public Reptile(String name, int numOfLeg) {
        super(name, numOfLeg);
        System.out.println("I am Reptile");
    }
    
    public abstract void crawl();
}
