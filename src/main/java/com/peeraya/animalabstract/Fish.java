/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.animalabstract;

/**
 *
 * @author Administrator
 */
public class Fish extends AquaticAnimal {
    
    private String nickname;

    public Fish(String nickname) {
        super("Fish");
        this.nickname = nickname;
    }
       
    @Override
    public void swim() {
        System.out.println(name + ": " + nickname + " swim");
    }

    @Override
    public void eat() {
        System.out.println(name + ": " + nickname + " eat");
    }

    @Override
    public void walk() {
        System.out.println(name + ": " + nickname + " don't walk");
    }

    @Override
    public void speak() {
        System.out.println(name + ": " + nickname + " speak");
    }

    @Override
    public void sleep() {
        System.out.println(name + ": " + nickname + " sleep");
    }
}
