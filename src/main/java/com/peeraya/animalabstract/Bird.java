/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.animalabstract;

/**
 *
 * @author Administrator
 */
public class Bird extends Poultry {
    
    private String nickname;

    public Bird(String nickname) {
        super("Bird", 2);
        this.nickname = nickname;
    }
    
    @Override
    public void fly() {
        System.out.println(name + ": " + nickname + " fly");
    }

    @Override
    public void eat() {
        System.out.println(name + ": " + nickname + " eat");
    }

    @Override
    public void walk() {
        System.out.println(name + ": " + nickname + " walk");
    }

    @Override
    public void speak() {
        System.out.println(name + ": " + nickname + " speak");
    }

    @Override
    public void sleep() {
        System.out.println(name + ": " + nickname + " sleep");
    }
}
